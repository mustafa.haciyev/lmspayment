package com.example.lmspayment.service;

import com.example.lmspayment.dto.ExpenseDto;

public interface ExpenseService {

    void saveExpense(ExpenseDto expenseDto);
}
