package com.example.lmspayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LmsPaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(LmsPaymentApplication.class, args);
	}

}
